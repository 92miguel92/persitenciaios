//
//  ViewController.swift
//  PersistenciaIOS
//
//  Created by Master Móviles on 21/12/2016.
//  Copyright © 2016 Master Móviles. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UITextViewDelegate {

    var fechaEdicion: Date?
    
    @IBOutlet weak var pensamientoText: UITextView!
    @IBOutlet weak var fechaLb: UILabel!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.pensamientoText.delegate = self;
        let nc = NotificationCenter.default
        nc.addObserver(self, selector:#selector(self.vamosABackground), name:NSNotification.Name.UIApplicationDidEnterBackground, object: nil)
        let urls = FileManager.default.urls(for:.documentDirectory, in:.userDomainMask)
        if(urls.count>0) {
            let docDir = urls[0]
            print("El directorio 'Documents' es \(docDir)")
            var NSdiccionario : NSDictionary?
            print("PATH: \(docDir.path+"Pensamiento.plist")")
                NSdiccionario = NSDictionary(contentsOfFile: docDir.path+"/Pensamiento.plist")
                if let unwrappedDict = NSdiccionario {
                    if let fuente = unwrappedDict["textoMensaje"],
                        let colorFondo = unwrappedDict["fechaEdicion"] {
                        pensamientoText.text = String(describing: fuente)
                        fechaLb.text = String(describing: colorFondo)
                    }
                }
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    override func touchesEnded(_ touches: Set<UITouch>, with: UIEvent?) {

        self.pensamientoText.resignFirstResponder()
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        print("acabamos de editar")
        self.fechaEdicion = Date()
        self.fechaLb.text = DateFormatter.localizedString(from: self.fechaEdicion!, dateStyle: .short, timeStyle: .short)
    }
    
    func vamosABackground() {
        print("Nos notifican que vamos a background");
        let urls = FileManager.default.urls(for:.documentDirectory, in:.userDomainMask)
        let diccionario : [String : Any] = [
            "textoMensaje" : pensamientoText.text,
            "fechaEdicion" : fechaLb.text!
        ]
        if(urls.count>0) {
            let docDir = urls[0]
            print("El directorio 'Documents' es \(docDir)")
            let urlPlist = docDir.appendingPathComponent("Pensamiento.plist")
            let NSdiccionario = diccionario as NSDictionary
            NSdiccionario.write(to: urlPlist, atomically: true)
        }
        else {
            print("error al buscar el directorio 'Documents'")
        }
    }
}

